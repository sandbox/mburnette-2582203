# Admin Custom Files - Drupal module

This module was created to allow Drupal site builders the option of adding a custom CSS and JS file to the admin areas of a Drupal website. These added styles and scripts can then be used to fine-tune the admin interface without the need for a completely separate admin theme or subtheme.

## Files

- admin_custom_files.info
- admin_custom_files.module
- admin_custom_files.css
- admin_custom_files.js